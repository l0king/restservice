package myapplication.payment;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import myapplication.customer.Customer;

import javax.persistence.*;

@Entity
@Table(name = "payments")
public class Payment {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "checknumber")
  private String checkNumber;
  @Column(name = "paymentdate")
  private java.sql.Date paymentDate;
  private double amount;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "customernumber", columnDefinition = "customernumber")
  @JsonIgnoreProperties("payments")
  private Customer customer;

  public String getCheckNumber() {
    return checkNumber;
  }

  public void setCheckNumber(String checkNumber) {
    this.checkNumber = checkNumber;
  }


  public java.sql.Date getPaymentDate() {
    return paymentDate;
  }

  public void setPaymentDate(java.sql.Date paymentDate) {
    this.paymentDate = paymentDate;
  }


  public double getAmount() {
    return amount;
  }

  public void setAmount(double amount) {
    this.amount = amount;
  }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
}
