package myapplication.payment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/payment")
public class PaymentController {
        @Autowired
        private PaymentRepository paymentRepository;

        @GetMapping("/all")
        public @ResponseBody
        Iterable<Payment> getAllPayments() {
            return paymentRepository.findAll();
        }

    }
