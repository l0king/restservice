package myapplication.customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;


@Controller
@RequestMapping("/customer")
public class CustomerController {
    @Autowired
    private CustomersRepository customersRepository;

    @GetMapping("/all")
    public @ResponseBody Iterable<Customer> getAllCustomers() {
        return customersRepository.findAll();
    }
    @GetMapping(path = "/{name}")
    public @ResponseBody Optional<Customer> getCustomer(@PathVariable String name){
        return customersRepository.findByCustomerName(name);
    }
}
