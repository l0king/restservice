package myapplication.customer;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CustomersRepository extends CrudRepository<Customer, Long> {
    Optional<Customer> findByCustomerName(String name);
}
